-- Writeup at https://work.njae.me.uk/2023/12/03/advent-of-code-2023-day-03/
import AoC

import Data.Char
import Data.List
import Linear (V2(..), (^+^))
import Data.Array.IArray

type Position = V2 Int -- r, c
type Engine = Array Position Char
type Region = [Position]

data NumberSeek = 
  NumberSeek { positions :: Region
             , foundNumbers :: [Region]
             } deriving (Show)


main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- readFile dataFileName
      let engine = mkEngine text
      let allNums = findNumbers engine
      let symbols = findSymbols engine
      let partNums = filter (touchRegion symbols) allNums
      -- print partNums
      -- print engine
      -- print $ findNumbers engine
      print $ part1 engine partNums
      print $ part2 engine partNums


part1, part2 :: Engine -> [Region] -> Int
part1 engine partNums = sum partNumValues
  where partNumValues = map (readNumber engine) partNums

part2 engine partNums = sum $ fmap product gearRatios
  where stars = findStars engine
        touchingStars = fmap (possibleGears partNums) stars
        gears = filter ((==) 2 . length) touchingStars
        gearRatios = fmap (fmap (readNumber engine)) gears

mkEngine :: String -> Engine
mkEngine text = grid
  where rows = lines text
        r = length rows - 1
        c = (length $ head rows) - 1
        grid = listArray ((V2 0 0), (V2 r c)) $ concat rows

isEngineSymbol :: Char -> Bool
isEngineSymbol c = (not $ isDigit c) && (c /= '.')

findNumbers :: Engine -> [Region]
findNumbers engine = numbers
  where ((V2 r1 _), (V2 r2 _)) = bounds engine
        rows = [r1..r2]
        numbers = concatMap (foundNumbers . (findNumbersInRow engine)) rows

findNumbersInRow :: Engine -> Int -> NumberSeek
findNumbersInRow engine r
  | not $ null positions = 
      NumberSeek [] ((reverse $ positions):foundNumbers)
  | otherwise = finalSeek
  where finalSeek@NumberSeek{..} = 
            foldl' (buildNumber engine) 
                   (NumberSeek [] []) 
                   $ range $ rowBounds engine r
  
buildNumber :: Engine -> NumberSeek -> Position -> NumberSeek
buildNumber engine NumberSeek{..} p 
  | (not $ null positions) && isDigit c = NumberSeek (p:positions) foundNumbers
  | (not $ null positions) && not (isDigit c) = NumberSeek [] ((reverse positions):foundNumbers)
  | (null positions) && isDigit c = NumberSeek [p] foundNumbers
  | otherwise = NumberSeek [] foundNumbers
  where c = engine ! p

rowBounds :: Engine -> Int -> (V2 Int, V2 Int)
rowBounds engine r = (V2 r c1, V2 r c2)
  where (V2 _ c1, V2 _ c2) = bounds engine

neighbours :: Position -> Region
neighbours p = [p ^+^ V2 dr dc | dr <- [-1..1], dc <- [-1..1]
                               , (dr, dc) /= (0, 0) ]


touchPoint :: Region -> Position -> Bool
touchPoint region point = not $ null $ intersect region $ neighbours point

touchRegion :: Region -> Region -> Bool
touchRegion region1 region2 = any (touchPoint region2) region1


readNumber :: Engine -> Region -> Int
readNumber engine ps = read $ map (engine !) ps

findSymbols :: Engine -> Region
findSymbols engine = filter (isEngineSymbol . (engine !)) $ indices engine

findStars :: Engine -> Region
findStars engine = filter ((==) '*' . (engine !)) $ indices engine

possibleGears :: [Region] -> Position -> [Region]
possibleGears nums star = filter (flip touchPoint star) nums
