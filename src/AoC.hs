module AoC ( getDataFileName ) where

import System.Environment

getDataFileName :: IO String
getDataFileName =
  do args <- getArgs
     progName <- getProgName
     let baseDataName =  if null args
                         then progName
                         else head args 
     let dataFileName = "../data/" ++ baseDataName ++ ".txt"
     return dataFileName
