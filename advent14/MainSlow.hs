-- Writeup at https://work.njae.me.uk/2023/12/18/advent-of-code-2023-day-14/

import AoC
import Data.List
import Data.Semigroup
import Data.Monoid
import qualified Data.Map.Strict as M

data Element = Empty | Cube | Round deriving (Show, Eq, Ord)
type Grid = [[Element]]

type Cache = M.Map Grid Int


main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- readFile dataFileName
      let grid = transpose $ fmap (fmap readElem) $ lines text
      -- print $ showGrid grid
      print $ part1 grid
      print $ part2 grid

part1, part2 :: Grid -> Int
part1 grid = scoreGrid grid'
  where grid' = rollToCompletion grid

part2 grid = scoreGrid finalGrid
  where (grid', cache, repeatEnd) = findRepeat grid
        repeatStart = cache M.! grid'
        repeatLen = repeatEnd - repeatStart
        finalIndex = ((1e9 - repeatStart) `mod` repeatLen) + repeatStart
        (finalGrid, _) = M.findMin $ M.filter (== finalIndex) cache

readElem :: Char -> Element
readElem '.' = Empty
readElem '#' = Cube
readElem 'O' = Round

rollToCompletion :: Grid -> Grid
rollToCompletion grid = fst $ head $ dropWhile (uncurry (/=)) $ zip states $ tail states
  where states = iterate rollGrid grid

rollGrid :: Grid -> Grid
rollGrid = fmap roll

roll :: [Element] -> [Element]
roll [] = []
roll (l:ls) = rs ++ [r]
  where (rs, r) = foldl' rollStep ([], l) ls

rollStep :: ([Element], Element) -> Element -> ([Element], Element)
rollStep (handled, Empty) Round = (handled ++ [Round], Empty)
rollStep (handled, target) source = (handled ++ [target], source)

scoreGrid :: Grid -> Int
scoreGrid grid = sum $ fmap scoreRow indexedGrid
  where indexedGrid = zip [1..] $ reverse $ transpose grid
        scoreRow (i, r) = i * (length $ filter (== Round) r)

rotate1 :: Grid -> Grid
rotate1 = transpose . fmap reverse

rollCycle :: Grid -> Grid
rollCycle = appEndo (stimes 4 (Endo rotate1 <> Endo rollToCompletion))

findRepeat :: Grid -> (Grid, Cache, Int)
findRepeat grid = head $ dropWhile test $ iterate go (grid, M.empty, 0)
  where test (g, c, _) = g `M.notMember` c
        go (g, c, i) = (rollCycle g, M.insert g i c, (i + 1))

showGrid :: Grid -> String
showGrid grid = unlines $ fmap (fmap showElem) $ transpose grid
  where showElem Empty = '.'
        showElem Cube = '#'
        showElem Round = 'O'
