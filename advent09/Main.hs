-- Writeup at https://work.njae.me.uk/2023/12/09/advent-of-code-2023-day-09/

import AoC
import Data.List
import Prelude hiding (seq)

type Sequence = [[Int]] 

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- readFile dataFileName
      let histories = readInput text
      -- print histories
      let seqs = fmap expand histories
      -- print seqs
      -- let seqs' = fmap extend seqs
      -- print seqs'
      print $ part1 seqs
      let rseqs = fmap (expand . reverse) histories
      print $ part1 rseqs

part1 :: [Sequence] -> Int
part1 = sum . fmap (evaluate . extend)


readInput :: String -> [[Int]]
readInput = fmap (fmap read . words) . lines

expand :: [Int] -> Sequence
expand seq = unfoldr go seq
  where go xs
          | all (== 0) xs = Nothing
          | otherwise = Just (xs, differences xs)

differences :: [Int] -> [Int]
differences xs = zipWith (-) (tail xs) xs

extend :: Sequence -> Sequence
extend = fst . foldr extendRow ([], 0)

extendRow :: [Int] -> ([[Int]], Int) -> ([[Int]], Int)
extendRow row (seq, n) = ((row ++ [n']) : seq, n')
  where n' = last row + n

evaluate :: Sequence -> Int
evaluate = last . head
