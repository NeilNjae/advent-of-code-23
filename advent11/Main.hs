-- Writeup at https://work.njae.me.uk/2023/12/11/advent-of-code-2023-day-11/

import AoC

-- import Data.List
-- import Data.Maybe
import Linear (V2(..), (^+^), (^-^))
import qualified Data.Set as S

type Position = V2 Int -- r, c

type Galaxies = S.Set Position

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- readFile dataFileName
      let galaxies = mkGalaxies text
      -- print galaxies
      -- print $ expandGalaxies galaxies 10

      -- print $ allDist $ expandGalaxies 2 galaxies
      -- print $ allDist $ expandGalaxies (10^6) galaxies

      print $ part1 galaxies
      print $ part2 galaxies

part1, part2 :: Galaxies -> Int
part1 = allDistances . expandGalaxies 2
part2 = allDistances . expandGalaxies 10e6

allDistances :: Galaxies -> Int
allDistances gs = snd $ S.foldl' addGalaxy (S.empty, 0) gs
  where addGalaxy (seen, d) new = 
            (S.insert new seen, S.foldl' (addDist new) d seen)
        addDist g1 d g2 = d + distance g1 g2

distance :: Position -> Position -> Int
distance g1 g2 = abs dr + abs dc
  where (V2 dr dc) = g1 ^-^ g2


expandGalaxies :: Int -> Galaxies -> Galaxies
expandGalaxies scale galaxies = galaxies''
  where er = emptyRows galaxies
        galaxies' = expandRows galaxies er scale
        ec = emptyCols galaxies'
        galaxies'' = expandCols galaxies' ec scale

emptyRows, emptyCols :: Galaxies -> [Int]
emptyRows galaxies = [ r | r <- [0..r1] , S.null $ onRow galaxies r ]
  where r1 = S.findMax $ S.map (\(V2 r _) -> r) galaxies
emptyCols galaxies = [ c | c <- [0..c1] , S.null $ onCol galaxies c ]
  where c1 = S.findMax $ S.map (\(V2 _ c) -> c) galaxies

onRow, onCol :: Galaxies -> Int -> Galaxies
onRow galaxies r = S.filter (\(V2 r' _) -> r == r') galaxies
onCol galaxies c = S.filter (\(V2 _ c') -> c == c') galaxies

expandRows, expandCols :: Galaxies -> [Int] -> Int -> Galaxies
expandRows galaxies expansions scale = foldr (shiftRows scale) galaxies expansions
expandCols galaxies expansions scale = foldr (shiftCols scale) galaxies expansions

shiftRows, shiftCols :: Int -> Int -> Galaxies -> Galaxies
shiftRows scale n galaxies = S.union small large'
  where (small, large) = S.partition (\(V2 r _) -> r < n) galaxies
        large' = S.map (^+^ (V2 (scale - 1) 0)) large
shiftCols scale n galaxies = S.union small large'
  where (small, large) = S.partition (\(V2 _ c) -> c < n) galaxies
        large' = S.map (^+^ (V2 0 (scale - 1))) large

-- reading the map

mkGalaxies :: String -> Galaxies
mkGalaxies text = S.fromList [ V2 r c | r <- [0..maxR], c <- [0..maxC]
                                      , rows !! r !! c == '#'
                             ]
  where rows = lines text
        maxR = length rows - 1
        maxC = (length $ head rows) - 1
