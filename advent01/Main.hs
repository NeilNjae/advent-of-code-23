-- Writeup at https://work.njae.me.uk/2023/12/01/advent-of-code-2023-day-01/

import AoC
import Data.Char
import Data.List

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      cals <- readFile dataFileName
      let calibrations = lines cals
      print $ part1 calibrations
      print $ part2 calibrations

part1 :: [String] -> Int
-- part1 calibrations = sum $ fmap getCalibration calibrations
part1  = sum . (fmap getCalibration)

part2 :: [String] -> Int
part2 calibrations = sum $ fmap (getCalibration . replaceNums) calibrations

getCalibration :: String -> Int
getCalibration calibration = read [head digits, last digits]
  where digits = filter isDigit calibration

replaceNums :: String -> String
replaceNums haystack = reverse $ foldl' go "" $ tails haystack
  where go acc [] = acc
        go acc xs 
          | "one"   `isPrefixOf` xs = '1' : acc
          | "two"   `isPrefixOf` xs = '2' : acc
          | "three" `isPrefixOf` xs = '3' : acc
          | "four"  `isPrefixOf` xs = '4' : acc
          | "five"  `isPrefixOf` xs = '5' : acc
          | "six"   `isPrefixOf` xs = '6' : acc
          | "seven" `isPrefixOf` xs = '7' : acc
          | "eight" `isPrefixOf` xs = '8' : acc
          | "nine"  `isPrefixOf` xs = '9' : acc
          | isDigit (head xs) = (head xs) : acc
          | otherwise = acc
