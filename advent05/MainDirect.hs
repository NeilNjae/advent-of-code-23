-- Writeup at https://work.njae.me.uk/2023/12/05/advent-of-code-2023-day-05/

import AoC
import Data.Text (Text)
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text -- hiding (take)
-- import Control.Applicative
import qualified Data.Map.Strict as M
import Data.Map.Strict ((!))
import Data.Maybe
import Data.List.Split (chunksOf)

type Almanac = M.Map String AMap
data AMap = AMap String [Rule] deriving (Eq, Show)
data Rule = Rule Int Int Int deriving (Eq, Show)
data Requirement = Requirement String [Int] deriving (Eq, Show)


main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- TIO.readFile dataFileName
      let (seeds, almanac) = successfulParse text
      print $ part1 almanac seeds
      print $ part2 almanac seeds

part1, part2 :: Almanac -> [Int] -> Int
part1 = lowestLocation

part2 almanac seeds = lowestLocation almanac $ expandRanges seeds

lowestLocation :: Almanac -> [Int] -> Int
lowestLocation almanac seeds = minimum locations
  where Requirement _ locations = followRequirements almanac $ Requirement "seed" seeds

followRequirements :: Almanac -> Requirement -> Requirement
followRequirements _ req@(Requirement "location" vals) = req
followRequirements almanac (Requirement name vals) = 
  followRequirements almanac newReq
  where aMap = almanac ! name
        newReq = useAMap aMap vals


useRule :: Int -> Rule -> Maybe Int
useRule x (Rule dest src rl)
  | x >= src && x < (src + rl) = Just (x + dest - src)
  | otherwise = Nothing

useRules :: [Rule] -> Int -> Int
useRules rs x 
  | null ruleResults = x
  | otherwise = head ruleResults
  where ruleResults = catMaybes $ fmap (useRule x) rs

useAMap :: AMap -> [Int] -> Requirement
useAMap (AMap d rs) xs = Requirement d $ fmap (useRules rs) xs


expandRanges :: [Int] -> [Int]
expandRanges seeds = concatMap expandRange ranges
  where ranges = chunksOf 2 seeds
        expandRange [s, l] = [s..(s + l - 1)]


-- Parse the input file

problemP :: Parser ([Int], Almanac)
seedsP :: Parser [Int]
almanacP :: Parser Almanac
aMapP :: Parser (String, AMap)
aMapHeaderP :: Parser (String, String)
rulesP :: Parser [Rule]
ruleP :: Parser Rule
numbersP :: Parser [Int]
nameP :: Parser String
blankLineP :: Parser ()

problemP = (,) <$> (seedsP <* blankLineP) <*> almanacP

seedsP = "seeds: " *> numbersP

almanacP = M.fromList <$> (aMapP `sepBy` blankLineP)

aMapP = aMapify <$> aMapHeaderP <*> rulesP
aMapHeaderP = (,) <$> nameP <* "-to-" <*> nameP <* " map:" <* endOfLine

rulesP = ruleP `sepBy` endOfLine
ruleP = Rule <$> decimal <* space <*> decimal <* space <*> decimal

numbersP = decimal `sepBy` skipSpace
nameP = many1 letter

blankLineP = endOfLine *> endOfLine

aMapify :: (String, String) -> [Rule] -> (String, AMap)
aMapify (s, d) rs = (s, AMap d rs)

successfulParse :: Text -> ([Int], Almanac)
successfulParse input = 
  case parseOnly problemP input of
    Left  _err -> ([], M.empty) -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right matches -> matches
