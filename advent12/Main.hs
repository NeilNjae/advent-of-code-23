-- Writeup at https://work.njae.me.uk/2023/12/15/advent-of-code-2023-day-12/

import AoC
import Data.Text (Text)
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text hiding (take, takeWhile)
import Control.Applicative
import Data.List
import qualified Data.Map.Strict as M

data Spring = Unknown | Damaged | Operational deriving (Show, Eq, Ord)
data Record = Record [Spring] [Int] deriving (Show, Eq, Ord)

type Cache = M.Map Record Int


main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- TIO.readFile dataFileName
      let records = successfulParse text
      -- print records
      print $ part1 records
      print $ part2 records

part1, part2 :: [Record] -> Int
part1 = sum . fmap countViable
part2 = sum . fmap (countViable . unfoldRecord)

unfoldRecord :: Record -> Record
unfoldRecord (Record springs signature) = Record uSprings uSignature
  where uSprings = intercalate [Unknown] $ replicate 5 springs
        uSignature = concat $ replicate 5 signature

countViable :: Record -> Int
countViable record = table M.! record
  where table0 = initialCache record
        table = fillTable table0 record

initialCache :: Record -> Cache
initialCache (Record springs signature) = M.union lastOperational cache0
  where cache0 = M.union sprs sigs
        sprs = M.fromList $ fmap (\s -> (Record s [], 0)) $ tails springs
        sigs = M.fromList $ fmap (\g -> (Record [] g, 0)) $ tails signature
        lastOperationalChunk = 
          reverse $ takeWhile isPossOperational $ reverse springs
        lastOperational = 
          M.fromList $ fmap (\s -> (Record s [], 1)) $ tails lastOperationalChunk

fillTableCell, fillTableSigs, fillTable :: Cache -> Record -> Cache
fillTableCell table record
  | record `M.member` table = table
  | otherwise = M.insert record (opN + signN) table
  where (Record springs@(s:ss) signatures@(g:gs)) = record
        opN = if (isPossOperational s) then table M.! (Record ss signatures) else 0
        signN = if (possibleDamagedChunk springs g) then table M.! (Record (drop (g + 1) springs) gs) else 0

fillTableSigs table (Record springs signatures) = foldr (\gs t -> fillTableCell t (Record springs gs)) table $ tails signatures

fillTable table (Record springs signatures) = foldr (\ss t -> fillTableSigs t (Record ss signatures)) table $ tails springs 

isPossOperational :: Spring -> Bool
isPossOperational Operational = True
isPossOperational Unknown = True
isPossOperational _ = False

isPossDamaged :: Spring -> Bool
isPossDamaged Damaged = True
isPossDamaged Unknown = True
isPossDamaged _ = False

possibleDamagedChunk :: [Spring] -> Int -> Bool
possibleDamagedChunk springs n = 
  isDamagedChunk && ((null afterChunk) || (isPossOperational $ head afterChunk))
  where isDamagedChunk = (length $ filter isPossDamaged $ take n springs) == n
        afterChunk = drop n springs
  
-- Parse the input file

recordsP :: Parser [Record]
recordP :: Parser Record
springP :: Parser Spring

recordsP = recordP `sepBy` endOfLine
recordP = Record <$> many1 springP <* " " <*> decimal `sepBy` ","
springP = (Unknown <$ "?") <|> (Damaged <$ "#") <|> (Operational <$ ".")

successfulParse :: Text -> [Record]
successfulParse input = 
  case parseOnly recordsP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right matches -> matches
