-- Writeup at https://work.njae.me.uk/2023/12/25/advent-of-code-2023-day-20/

import Debug.Trace

import AoC
import Data.Text (Text)
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text hiding (take)
import Control.Applicative
import Data.List
import qualified Data.Map.Strict as M
import Data.Map.Strict ((!))
import qualified Data.Sequence as Q
import Data.Sequence ((|>), (><), Seq( (:|>), (:<|) ) ) 
import Control.Lens hiding (Level)
import Control.Monad.State.Strict
import Control.Monad.Reader
import Control.Monad.Writer
import Control.Monad.RWS.Strict
import Data.Function (on)

type Name = String

data Level = Low | High deriving (Show, Eq, Ord)
data Pulse = Pulse { _source :: Name, _level :: Level , _destination :: Name }
  deriving (Show, Eq, Ord)
makeLenses ''Pulse  

type Queue = Q.Seq Pulse

type Memory = M.Map Name Level

data Module = 
  Broadcast 
  | FlipFlop Bool 
  | Conjunction Memory
  | Untyped
  deriving (Show, Eq, Ord)

type Network = M.Map Name [Name]
type Modules = M.Map Name Module

data NetworkState = NetworkState { _modules :: Modules
                                 , _queue :: Queue
                                 }
  deriving (Show, Eq, Ord)
makeLenses ''NetworkState  

type NetworkHandler = RWS Network [Pulse] NetworkState

main :: IO ()
main = 
  do  dataFileName <- getDataFileName
      text <- TIO.readFile dataFileName
      let config = successfulParse text
      let (network, modules) = assembleNetwork config
      print $ part1 network modules
      print $ part2 network modules




part1, part2 :: Network -> Modules -> Int
part1 network modules = highs * lows 
  where (_, (highs, lows)) = 
         (!! 1000) $ iterate (pressAndEvaluate network part1Extractor) (state0, (0, 0))
        state0 = NetworkState modules Q.empty
part2 network modules = foldl' lcm 1 cycleLengths
  where (_, lxPulses) = 
         (!! 10000) $ iterate (pressAndEvaluate network part2Extractor) (state0, [(0, [])])
        state0 = NetworkState modules Q.empty
        lxHighs = filter (not . null . snd) lxPulses
        cycleLengths = fmap ((fst . head) . sort) $ 
                          groupBy ((==) `on` (_source . snd)) $ 
                          sortBy (compare `on` (\(_, p) -> p ^. source)) $ 
                          fmap (\(n, ps) -> (n, head ps)) lxHighs 

pressAndEvaluate :: Network -> (a -> [Pulse] -> a) -> (NetworkState, a) -> (NetworkState, a)
pressAndEvaluate network resultExtractor (state0, result0) = (state1, result1)
  where (state1, pulses) = buttonPress network state0
        result1 = resultExtractor result0 pulses

part1Extractor :: (Int, Int) -> [Pulse] -> (Int, Int)
part1Extractor (highs, lows) pulses = (highs + length hs, lows + length ls)
  where (hs, ls) = partition ((== High) . _level) pulses

part2Extractor :: [(Int, [Pulse])] -> [Pulse] -> [(Int, [Pulse])]
part2Extractor allRs@((i, _):rs) pulses = (i + 1, lxPulses) : allRs
  where lxPulses = filter catchLx pulses
        catchLx (Pulse _ High "lx") = True
        catchLx _ = False

buttonPress :: Network -> NetworkState -> (NetworkState, [Pulse])
buttonPress network state = 
  execRWS handlePulses network (state & queue .~ pulse0)
  where pulse0 = Q.singleton $ Pulse "button" Low "broadcaster"


handlePulses :: NetworkHandler ()
handlePulses = 
  do  pulses <- gets _queue
      case pulses of
        Q.Empty -> return ()
        (p :<| ps) -> 
          do modify (\s -> s & queue .~ ps)
             handlePulse p
             handlePulses

handlePulse :: Pulse -> NetworkHandler ()
handlePulse p@(Pulse _ _ destination) = 
  do  mdl <- gets ((! destination) . _modules)
      outGoings <- asks (! destination)
      let (mdl', maybeLevel) = processPulse p mdl
      modify (\s -> s & modules . ix destination .~ mdl')
      tell [p]
      case maybeLevel of
        Nothing -> return ()
        Just level' -> 
          do let newPulses = fmap (Pulse destination level') outGoings
             modify (\s -> s & queue %~ (>< (Q.fromList newPulses)))

processPulse :: Pulse -> Module -> (Module, Maybe Level)
-- processPulse p m | trace ((show p) ++ " " ++ (show m) ) False = undefined
processPulse (Pulse _ l _) Broadcast = (Broadcast, Just l)
processPulse (Pulse _ Low _) (FlipFlop False) = (FlipFlop True, Just High)
processPulse (Pulse _ Low _) (FlipFlop True) = (FlipFlop False, Just Low)
processPulse (Pulse _ High _) (FlipFlop s) = (FlipFlop s, Nothing)
processPulse (Pulse s l _) (Conjunction memory) = 
  (Conjunction memory', Just outLevel)
  where memory' = M.insert s l memory
        outLevel = if all (== High) $ M.elems memory' then Low else High
processPulse _ Untyped = (Untyped, Nothing)

-- Assemble the network

assembleNetwork :: [((Module, Name), [Name])] -> (Network, Modules)
assembleNetwork config = (network, modules)
  where (network, modules0) = mkNetwork config
        modules1 = M.union (mkModules config) modules0
        modules = addConjunctionMemory network modules1

mkNetwork :: [((Module, Name), [Name])] -> (Network, Modules)
mkNetwork config = (net, mods) 
  where net = M.fromList $ fmap (\((_, n), ds) -> (n, ds)) config
        mods = M.fromList $ concatMap (\(_, ds) -> fmap (\d -> (d, Untyped)) ds) config

mkModules :: [((Module, Name), [Name])] -> Modules
mkModules = M.fromList . fmap (\((m, n), _) -> (n, m))

addConjunctionMemory :: Network -> Modules -> Modules
addConjunctionMemory network modules = 
  M.foldlWithKey addMemory modules network

addMemory :: Modules -> Name -> [Name] -> Modules
addMemory modules source connections = 
  foldl' (addOneMemory source) modules connections

addOneMemory :: Name -> Modules -> Name -> Modules
addOneMemory source modules destination =
  case modules ! destination of
    Conjunction memory -> 
      M.insert destination 
               (Conjunction $ M.insert source Low memory) 
               modules
    _ -> modules

showDot :: Network -> String
showDot network = 
  "digraph {\n" ++ (concatMap showDotLine $ M.toList network) ++ "\n}"
  where showDotLine (source, destinations) = 
          concatMap (\d -> source ++ " -> " ++ d ++ ";\n") destinations

-- Parse the input file

configLinesP :: Parser [((Module, Name), [Name])]
configLineP :: Parser ((Module, Name), [Name])
moduleP, broadcastP, flipFlopP, conjunctionP :: Parser (Module, Name)
-- namesP :: Parser [Name]
nameP :: Parser Name

configLinesP = configLineP `sepBy` endOfLine
configLineP = (,) <$> moduleP <* " -> " <*> nameP `sepBy` ", "

moduleP = broadcastP <|> flipFlopP <|> conjunctionP

broadcastP = (Broadcast, "broadcaster") <$ "broadcaster"
flipFlopP = (FlipFlop False, ) <$ "%" <*> nameP
conjunctionP = (Conjunction M.empty, ) <$ "&" <*> nameP
  
-- namesP = nameP `sepBy` ", "
nameP = many1 letter

successfulParse :: Text -> [((Module, Name), [Name])]
successfulParse input = 
  case parseOnly configLinesP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right monkeys -> monkeys
